package step_Definations;

import commonMethod.Trigger_API_GetMethod;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.path.json.JsonPath;

import java.util.List;

import org.testng.Assert;
import reqRepository.endPoints;

public class Get_API_StepDefination {
	String Get_Endpoint;
	int res_statusCode;
	String res_body;

	@Given("Enter name and job for Get_API in request Body")
	public void enter_name_and_job_for_get_api_in_request_body() {
		Get_Endpoint = endPoints.getendpoint();
//    throw new io.cucumber.java.PendingException();
	}

	@When("Send the Get request with request body")
	public void send_the_get_request_with_request_body() {
		res_statusCode = Trigger_API_GetMethod.extract_Status_Code(Get_Endpoint);
		res_body = Trigger_API_GetMethod.extract_response_body(Get_Endpoint);
//    throw new io.cucumber.java.PendingException();
	}

	@Then("Success response is received {int} as status code for Get_API")
	public void success_response_is_received_as_status_code_for_get_api(Integer int1) {
		Assert.assertEquals(res_statusCode, 200);
//    throw new io.cucumber.java.PendingException();
	}

	@Then("Response body for Get_API as per API guide document")
	public void response_body_for_get_api_as_per_api_guide_document() {
		String exp_id_array[] = { "7", "8", "9", "10", "11", "12" };
		String exp_email_array[] = { "michael.lawson@reqres.in", "lindsay.ferguson@reqres.in", "tobias.funke@reqres.in",
				"byron.fields@reqres.in", "george.edwards@reqres.in", "rachel.howell@reqres.in" };
		String exp_first_name_array[] = { "Michael", "Lindsay", "Tobias", "Byron", "George", "Rachel" };
		String exp_last_name_array[] = { "Lawson", "Ferguson", "Funke", "Fields", "Edwards", "Howell" };
		String exp_avatar_array[] = { "https://reqres.in/img/faces/7-image.jpg",
				"https://reqres.in/img/faces/8-image.jpg", "https://reqres.in/img/faces/9-image.jpg",
				"https://reqres.in/img/faces/10-image.jpg", "https://reqres.in/img/faces/11-image.jpg",
				"https://reqres.in/img/faces/12-image.jpg" };

		JsonPath jsp_res = new JsonPath(res_body);
		List<Object> res_data = jsp_res.getList("data");
		// to store the list of array object from data(responseBody)...List is type of
		// collection.
		int count = res_data.size();
		// count = 6
		for (int i = 0; i < count; i++) {

			String Exp_id = exp_id_array[i];// 7
			String res_id = jsp_res.getString("data[" + i + "].id");
			Assert.assertEquals(res_id, Exp_id);

			String Exp_email = exp_email_array[i];
			String res_email = jsp_res.getString("data[" + i + "].email");// data.email[i]...data.email[0],data.email[1]
			Assert.assertEquals(res_email, Exp_email);

			String Exp_first_name = exp_first_name_array[i];
			String res_first_name = jsp_res.getString("data[" + i + "].first_name");
			Assert.assertEquals(res_first_name, Exp_first_name);

			String Exp_last_name = exp_last_name_array[i];
			String res_last_name = jsp_res.getString("data[" + i + "].last_name");
			Assert.assertEquals(res_last_name, Exp_last_name);

			String Exp_avatar = exp_avatar_array[i];
			String res_avatar = jsp_res.getString("data[" + i + "].avatar");
			Assert.assertEquals(res_avatar, Exp_avatar);

		}
//    throw new io.cucumber.java.PendingException();
	}

}
