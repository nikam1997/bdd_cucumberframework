package step_Definations;

import java.io.IOException;
import java.time.LocalDateTime;

import commonMethod.Trigger_API_PutMethod;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.path.json.JsonPath;

import org.testng.Assert;
import reqRepository.PutRequestRepository;

public class Put_API_StepDefination {
	String req_body;
	String Put_Endpoint;
	int res_statusCode;
	String res_body;

	@Given("Enter name and job for Put_API in request Body")
	public void enter_name_and_job_for_put_api_in_request_body() throws IOException {
		req_body = PutRequestRepository.TC1_request();
		Put_Endpoint = PutRequestRepository.putendpoint();
//	    throw new io.cucumber.java.PendingException();
	}

	@When("Send the Put request with request body")
	public void send_the_put_request_with_request_body() {
		res_statusCode = Trigger_API_PutMethod.extract_Status_Code(req_body, Put_Endpoint);
		res_body = Trigger_API_PutMethod.extract_response_body(req_body, Put_Endpoint);
//	    throw new io.cucumber.java.PendingException();
	}

	@Then("Success response is received {int} as status code for Put_API")
	public void success_response_is_received_as_status_code_for_put_api(Integer int1) {
		Assert.assertEquals(res_statusCode, 200);
//	    throw new io.cucumber.java.PendingException();
	}

	@Then("Response body for Put_API as per API guide document")
	public void response_body_for_put_api_as_per_api_guide_document() {
		JsonPath jsp_res = new JsonPath(res_body);

		String res_name = jsp_res.getString("name");

		String res_job = jsp_res.getString("job");

//		System.out.println("Generated ID : " + res_id);

		String res_updatedAt = jsp_res.getString("updatedAt");

		String generatedDate = res_updatedAt.substring(0, 10);
//		System.out.println(generatedDate);

		LocalDateTime CurrentDate = LocalDateTime.now();

		String newDate = CurrentDate.toString();

		String updatedDate = newDate.substring(0, 10);

//		System.out.println(updatedDate);

		// object of JsonPath to parse the request body.

		JsonPath jsp_req = new JsonPath(req_body);
		String req_name = jsp_req.getString("name");

//		System.out.println(req_name);

		String req_job = jsp_req.getString("job");

//		System.out.println(req_job);

		// Validation

		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
		Assert.assertEquals(generatedDate, updatedDate);
//	    throw new io.cucumber.java.PendingException();
	}

}
