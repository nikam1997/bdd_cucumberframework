package step_Definations;

import org.testng.Assert;

import commonMethod.Trigger_API_DeleteMethod;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import reqRepository.endPoints;

public class Delete_API_StepDefination {
	String Delete_Endpoint;
	int res_statusCode;
@Given("Enter name and job Delete_API in request Body")
public void enter_name_and_job_delete_api_in_request_body() {
	Delete_Endpoint = endPoints.deleteEndPoint();
//    throw new io.cucumber.java.PendingException();
}
@When("Send the Delete request with request body")
public void send_the_delete_request_with_request_body() {
	res_statusCode = Trigger_API_DeleteMethod.extract_SC_Delete(Delete_Endpoint);
//    throw new io.cucumber.java.PendingException();
}
@Then("Success response is received {int} as status code Delete_API")
public void success_response_is_received_as_status_code_delete_api(Integer int1) {
	Assert.assertEquals(res_statusCode, 204);
//    throw new io.cucumber.java.PendingException();
}

}
