package step_Definations;

import java.io.IOException;
import java.time.LocalDateTime;

import org.testng.Assert;

import commonMethod.Trigger_API_PostMethod;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.path.json.JsonPath;
import reqRepository.PostRequestRepository;

public class Post_API_StepDefination {
	String req_body;
	String Post_Endpoint;
	int res_statusCode;
	String res_body;

@Given("Enter name and job in request Body")
public void enter_name_and_job_in_request_body() throws IOException {
	req_body = PostRequestRepository.TC1_request();
	Post_Endpoint = PostRequestRepository.postendpoint();
//    throw new io.cucumber.java.PendingException();
}
@When("Send the Post request with request body")
public void send_the_post_request_with_request_body() {
	res_statusCode = Trigger_API_PostMethod.extract_Status_Code(req_body, Post_Endpoint);
	res_body = Trigger_API_PostMethod.extract_response_body(req_body, Post_Endpoint);
//    throw new io.cucumber.java.PendingException();
}
@Then("Success response is received {int} as status code")
public void success_response_is_received_as_status_code(Integer int1) {
	Assert.assertEquals(res_statusCode, 201);
//    throw new io.cucumber.java.PendingException();
}
@Then("Response body as per API guide document")
public void response_body_as_per_api_guide_document() {
	JsonPath jsp_res = new JsonPath(res_body);

	String res_name = jsp_res.getString("name");

	String res_job = jsp_res.getString("job");

	int res_id = jsp_res.getInt("id");

	String res_createdAt = jsp_res.getString("createdAt");

	String generatedDate = res_createdAt.substring(0, 10);

	LocalDateTime CurrentDate = LocalDateTime.now();

	String newDate = CurrentDate.toString();

	String updatedDate = newDate.substring(0, 10);

	// object of JsonPath to parse the request body.

	JsonPath jsp_req = new JsonPath(req_body);
	String req_name = jsp_req.getString("name");

	String req_job = jsp_req.getString("job");

	// Validation

	Assert.assertEquals(res_name, req_name);
	Assert.assertEquals(res_job, req_job);
	Assert.assertNotNull(res_id);
	Assert.assertEquals(generatedDate, updatedDate);
//    throw new io.cucumber.java.PendingException();
}
}
