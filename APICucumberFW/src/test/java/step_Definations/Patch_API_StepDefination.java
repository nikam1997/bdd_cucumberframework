package step_Definations;

import java.io.IOException;
import java.time.LocalDateTime;

import commonMethod.Trigger_API_PatchMethod;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.path.json.JsonPath;

import org.testng.Assert;
import reqRepository.PatchRequestRepository;

public class Patch_API_StepDefination {
	String req_body;
	String Patch_Endpoint;
	int res_statusCode;
	String res_body;

	@Given("Enter name and job Patch_API in request Body")
	public void enter_name_and_job_patch_api_in_request_body() throws IOException {
		req_body = PatchRequestRepository.TC1_request();
		Patch_Endpoint = PatchRequestRepository.patchendpoint();
//    throw new io.cucumber.java.PendingException();
	}

	@When("Send the Patch request with request body")
	public void send_the_patch_request_with_request_body() {
		res_statusCode = Trigger_API_PatchMethod.extract_Status_Code(req_body, Patch_Endpoint);
		res_body = Trigger_API_PatchMethod.extract_response_body(req_body, Patch_Endpoint);
		// throw new io.cucumber.java.PendingException();
	}

	@Then("Success response is received {int} as status code Patch_API")
	public void success_response_is_received_as_status_code_patch_api(Integer int1) {
		Assert.assertEquals(res_statusCode, 200);
		// throw new io.cucumber.java.PendingException();
	}

	@Then("Response body Patch_API as per API guide document")
	public void response_body_patch_api_as_per_api_guide_document() {
		JsonPath jsp_res = new JsonPath(res_body);

		String res_name = jsp_res.getString("name");

		String res_job = jsp_res.getString("job");

		String res_updatedAt = jsp_res.getString("updatedAt");

		String generatedDate = res_updatedAt.substring(0, 10);
//	System.out.println(generatedDate);

		LocalDateTime CurrentDate = LocalDateTime.now();

		String newDate = CurrentDate.toString();

		String updatedDate = newDate.substring(0, 10);

//	System.out.println(updatedDate);

		// Create an object of JsonPath to parse the Request body.
		JsonPath jsp_req = new JsonPath(req_body);

		String req_name = jsp_req.getString("name");
//	System.out.println(req_name);

		String req_job = jsp_req.getString("job");

//	System.out.println(req_job);

		// Validation

		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
		Assert.assertEquals(generatedDate, updatedDate);
		// throw new io.cucumber.java.PendingException();
	}

}
