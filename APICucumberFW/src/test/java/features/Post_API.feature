Feature: Validate the Status Code and response body for Post API.

Scenario: Trigger the API with Valid request parameters and validate the Status Code
        Given Enter name and job in request Body
        When Send the Post request with request body
        Then Success response is received 201 as status code
        And Response body as per API guide document

