Feature: Validate the Status Code and response body for Patch API.

Scenario: Trigger the API with Valid request parameters and validate the Status Code
        Given Enter name and job Patch_API in request Body
        When Send the Patch request with request body
        Then Success response is received 200 as status code Patch_API
        And Response body Patch_API as per API guide document
