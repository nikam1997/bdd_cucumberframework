Feature: Validate the Status Code and response body for Delete API.

Scenario: Trigger the API with Valid request parameters and validate the Status Code
        Given Enter name and job Delete_API in request Body
        When Send the Delete request with request body
        Then Success response is received 204 as status code Delete_API
