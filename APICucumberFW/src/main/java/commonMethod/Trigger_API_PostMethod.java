package commonMethod;

import static io.restassured.RestAssured.given;

import reqRepository.PostRequestRepository;

public class Trigger_API_PostMethod extends PostRequestRepository{
	//FOR POST
	public static int extract_Status_Code(String requestBody, String URL) {
		int StatusCode = given().header("Content-Type", "application/json").body(requestBody).when().post(URL).then()
				.extract().statusCode();
		return StatusCode;
	}
	public static String extract_response_body(String requestBody, String URL) {
		
		String ResponseBody = given().header("Content-Type", "application/json").body(requestBody).when()
				.post(URL).then().extract().response().asString();
		return ResponseBody;
	
	}
	
}
