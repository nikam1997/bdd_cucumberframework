package commonMethod;

import static io.restassured.RestAssured.given;

import reqRepository.PatchRequestRepository;

public class Trigger_API_PatchMethod extends PatchRequestRepository {
	
	// FOR PATCH
	public static int extract_Status_Code(String requestBody, String URL) {
		int StatusCode = given().header("Content-Type", "application/json").body(requestBody).when().patch(URL).then()
				.extract().statusCode();
		return StatusCode;
	}

	public static String extract_response_body(String requestBody, String URL) {

		String ResponseBody = given().header("Content-Type", "application/json").body(requestBody).when().patch(URL)
				.then().extract().response().asString();
		return ResponseBody;

	}
}
