Feature: Validate the Status Code and response body for Get API.

Scenario: Trigger the API with Valid request parameters and validate the Status Code
        Given Enter name and job for Get_API in request Body
        When Send the Get request with request body
        Then Success response is received 200 as status code for Get_API
        And Response body for Get_API as per API guide document
