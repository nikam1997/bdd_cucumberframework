Feature: Validate the Status Code and response body for Put API.

Scenario: Trigger the API with Valid request parameters and validate the Status Code
        Given Enter name and job for Put_API in request Body
        When Send the Put request with request body
        Then Success response is received 200 as status code for Put_API
        And Response body for Put_API as per API guide document;
        
